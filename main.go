package main

import (
	"os"
	"flag"
)

var DEBUG bool

func main() {
	cert := flag.String("c", "", "Server certificate")
	key := flag.String("k", "", "Certificate key file")
	autocert := flag.Bool("a", false, "Whether to use autocert enrollment")
	flag.Parse()

	_, DEBUG = os.LookupEnv("DEBUG")
	serv := newServer(*autocert)

	//Start the server
	serv.setupRoutes()
	serv.startServer(*cert, *key, *autocert)
}
