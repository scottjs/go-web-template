#Params
param(
    [switch]$prod, #Adds -prod switch
    [switch]$docker, #Adds -docker switch
    [switch]$npm,
    [switch]$upload,
    [switch]$run
)

$env:GOOS = "windows"
$env:GOARCH = "amd64"
$mode = "dev"
$output_name = "server"
$name = "${output_name}-dev"
$env:DEBUG = $true


if ($prod -eq $true) {
    $mode = "prod"
    $name = $output_name
    Remove-Item env:DEBUG
}
$env:MODE = $mode
#Set base build directory
$baseDir = "build\"

#Set the full version identifer
$version = (git describe --tags --long)

if ($docker -eq $true) {
    $env:GOOS = "linux"
    #$baseDir += "docker\"
}

$output = "${baseDir}${output_name}" #Go binary output full path name
if ($env:GOOS -eq "windows") {
    $output += ".exe"
}

if ($npm -eq $true) {
    Write-Output "Running NPM Build"
    npm run "${mode}build"
    if ($mode -eq "prod") {
        Remove-Item -Include *.map .\static -Recurse
    }
}

Remove-Item $output

Write-Output "Running Go Build"
go build -o $output

#Revert to windows build, always
$env:GOOS = "windows"

if ($docker -eq $true) {
    $dockerFile = "${baseDir}Dockerfile"

    #Run the docker build process
    Write-Output "Running Docker Build"
    docker build -t "${name}:${version}" -t "${name}:latest" -f $dockerFile $baseDir

    if ($upload -eq $true) {
        #Export the container to a tar file
        Write-Output "Saving Docker Image to File"
        docker save -o build/"$name.tar" "${name}:${version}"

        #Use PSCP to upload the tar file to container host on Digital Ocean
        #Uses the keyfile loaded in PAgent
        #Write-Output "Uploading tar file to cloud Docker host"
        #pscp build/"$name.tar" user@domain:/path/

        #Clean up the tar file export
        Write-Output "Cleaning up"
        Remove-Item build/"$name.tar"
    }
}

if ($run -eq $true) {
    Write-Output "Running built file..."
    & $output
}
