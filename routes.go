package main

import (
	"net/http"
)

func (s *server) setupRoutes() {
	s.router.HandleFunc("/", homeHandler())
	s.router.PathPrefix("/static/").HandlerFunc(s.staticHandler())
}

func homeHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/static/index.html", 302)
	}
}


