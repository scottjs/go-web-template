package main

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	slg "gitlab.com/scottjs/sslogs"
	"golang.org/x/crypto/acme/autocert"
)

type server struct {
	router   *mux.Router
	httpserv *http.Server
}

func newServer(ac bool) *server {
	r := mux.NewRouter()
	var tlsc *tls.Config
	if ac == true {
		m := &autocert.Manager{
			Cache:      autocert.DirCache("/autocert"),
			Prompt:     autocert.AcceptTOS,
			Email:      "",
			HostPolicy: autocert.HostWhitelist(""),
		}
		tlsc = m.TLSConfig()
	}

	if tlsc == nil {
		tlsc = setupTLS()
	}
	h := &http.Server{
		Addr:         ":443",
		Handler:      r,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  5 * time.Second,
		TLSConfig:    tlsc,
	}

	return &server{
		router:   r,
		httpserv: h,
	}
}

func setupTLS() *tls.Config {
	return &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}
}

func (s *server) startServer(cert, key string, ac bool) {
	slg.Info("Starting server with TLS...")
	sTime := time.Now().UTC().Add(time.Second * 3)
	err := s.httpserv.ListenAndServeTLS(cert, key)
	if err != nil && time.Now().UTC().Before(sTime) {
		//Startup Error. Error occurred within 3 seconds of server launch
		slg.Errorf("Failed TLS start: %v", err)
		if DEBUG {
			s.httpserv.Addr = ":80"
			slg.Info("Starting server without TLS...")
			err = s.httpserv.ListenAndServe()
		}
	}
	if err != nil {
		slg.Errorf("Server error: %v", err)
	}
}

func (s *server) staticHandler() http.HandlerFunc {
	h := http.StripPrefix("/static", http.FileServer(http.Dir("static")))
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "private, max-age=43200")
		h.ServeHTTP(w, r)
	}
}
